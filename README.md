# KafkaDemo

#### 介绍
springboot嵌入Kafka的demo。
本项目源自B站的咕泡java视频。

#### 软件架构
java这块使用
springboot2.18+kafka2.0.0；

window使用的kafka版本为2.13-3.2.0
zookeeper使用的版本为：3.8.0


#### 安装教程

1.  先安装zookeeper，启动
2.  安装kafka，启动
3.  最好启动消费者与生产者控制界面


