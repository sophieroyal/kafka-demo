package com.sophie.common;

public abstract class KafkaContext {
    public static final String brokerList = "localhost:9092";
    public static final String topic = "heima";
    public static final String groupId = "group.heima";
}
