package com.sophie.chapter3;

import com.sophie.common.KafkaContext;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Properties;

public class ConsumerClientConfig extends KafkaContext {

    public static Properties initConfig() {
        Properties properties = new Properties();
        // Kafka集群地址列表
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerList);
        // key反序列化器
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        // value反序列化器
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        // 消费组
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);

        // Kafka消费片找不到消费的位移时.从什么处置开始消盥，默认:latest木尾开始消费   earliest：从头开始
        // properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        // 是否启用自动位移提交
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        return properties;
    }
}
